from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator 
from django.db import models

from calls.constants import TYPE_CHOICES


PHONE_VALIDATION = RegexValidator(
    regex='^[0-9]{10,11}$',
    message='Number Invalid! \
        The number format is AAXXXXXXXXX, where AA is the area code \
        and XXXXXXXXX is the phone number. \
        The phone number is composed of 8 or 9 digits.'
)

class CallRecord(models.Model):
    """
    This class represents a CallRecord
    A CallRecord can have information from a CallStartRecord or CallEndRecord

    :cvar str record_type: type of record, can be 'start' or 'end'
    :cvar datetime record_time: timestamp of record
    :cvar int call_id: call identifier, unique for each record pair
    :cvar str origin: phone number of origin call
    :cvar str destination: phone number of destination call
    
    The difference between CallStartRecord and CallEndRecord is that 
    the CallEndRecord don't has origin and destination number
    """
    record_type = models.CharField(max_length=5, choices=TYPE_CHOICES)
    record_time = models.DateTimeField()
    call_id = models.PositiveIntegerField()
    origin = models.CharField(
        max_length=11,
        blank=True,
        null=True,
        validators=[PHONE_VALIDATION]
    )
    destination = models.CharField(
        max_length=11,
        blank=True,
        null=True,
        validators=[PHONE_VALIDATION]
    )
    
    class Meta:
        ordering = ('call_id',)
        unique_together = ("record_type", "call_id")

    def __str__(self):
        return f'call_id: {self.call_id} \
            type: {self.record_type} at {self.record_time}'


class TelephoneCall(models.Model):
    """
    This class represents a TelephoneCall
    A CallRecord can have information from a CallStartRecord or CallEndRecord

    :cvar int call_id: call identifier, unique for each record pair
    :cvar str origin: phone number of origin call
    :cvar str destination: phone number of destination call
    :cvar date start_date: call's start date
    :cvar date start_time: call's start time
    :cvar duration duration: call's duration
    :cvar decimal price: call's price
    """
    call_id = models.PositiveIntegerField(unique=True)
    origin = models.CharField(max_length=11)
    destination = models.CharField(max_length=11)
    start_date = models.DateField(auto_now=False, auto_now_add=False)
    start_time = models.TimeField(auto_now=False, auto_now_add=False)
    duration = models.DurationField()
    price = models.DecimalField(max_digits=6, decimal_places=2)
   
    def __str__(self):
        return f'({self.call_id}) {self.origin} to {self.destination} \
        at {self.start_date} {self.start_time}'
