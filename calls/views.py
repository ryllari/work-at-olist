from datetime import datetime
from django.db.models import Sum
from rest_framework import status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ReadOnlyModelViewSet, ModelViewSet

from calls.models import CallRecord, TelephoneCall
from calls.serializers import (
    CallStartSerializer,
    CallEndSerializer,
    TelephoneCallSerializer
)


class CallRecordViewSet(ModelViewSet):
    """
    This view contains all data about CallStart and CallEnd records
    """

    http_method_names = [u'get', u'post', u'head', u'options']
    lookup_field = 'id'
    lookup_url_kwarg = 'id'
    queryset = CallRecord.objects.all()
    serializer_class = CallStartSerializer

    def create(self, request, *args, **kwargs):
        data = request.data
        record_type = data.get('record_type', None)
        if record_type == 'start':
            serializer = CallStartSerializer(data=data)
        elif record_type == 'end':
            serializer = CallEndSerializer(data=data)
        else:
            return Response('Unknown Type', status=status.HTTP_400_BAD_REQUEST)
        
        if serializer.is_valid():
            serializer.save()
            headers = self.get_success_headers(serializer.data)
            return Response(
                serializer.data,
                status=status.HTTP_201_CREATED,
                headers=headers
            )
    
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def list(self, request, *args, **kwargs):
        starts = self.queryset.filter(record_type='start')
        ends = self.queryset.filter(record_type='end')
        start_serial = CallStartSerializer(starts, many=True)
        end_serial = CallEndSerializer(ends, many=True)

        return Response(
            {
                'call_starts': start_serial.data,
                'call_ends': end_serial.data,
            }
        )

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        if instance.record_type == 'end':
            serializer = CallEndSerializer(instance)          
        else:
            serializer = self.get_serializer(instance)
        return Response(serializer.data)


class TelephoneCallViewSet(ReadOnlyModelViewSet):
    """
    This view contains all data about TelephoneCalls
    """

    lookup_field = 'call_id'
    lookup_url_kwarg = 'call_id'
    queryset = TelephoneCall.objects.all()
    serializer_class = TelephoneCallSerializer


class TelephoneBillView(APIView):
    """
    This view contains all data about the TelephoneBill
    """

    queryset = TelephoneCall.objects.all()

    def get(self, request, format=None):
        
        return Response(
            {
                'message': 'Provide Number to access the Telephone Bill'
            }
        )

    def post(self, request, format=None):
        origin = request.data.get('origin', None)
        period = request.data.get('period', None)

        if not origin:
            return Response(
                'Please, provide a valid origin!',
                status=status.HTTP_400_BAD_REQUEST
            )
        
        if period:
            try:
                period = period.split('/')
                month, year = int(period[0]), int(period[1])
                year_now = datetime.now().year
                if year > year_now or month > 12:
                    return Response(
                        'Please, provide a valid period! (format: MM/YYYY)',
                        status=status.HTTP_400_BAD_REQUEST
                    )
                
            except:
                return Response(
                    'Please, provide a valid period! (format: MM/YYYY)',
                    status=status.HTTP_400_BAD_REQUEST
                    )
        else:
            date = datetime.now()
            year, month = date.year, date.month - 1
            if month == 0:
                month = 12

        records = CallRecord.objects.filter(
                record_type='end',
                record_time__month=month,
                record_time__year=year,
                )

        month_end_ids = records.values_list('call_id', flat=True)
        data = TelephoneCall.objects.filter(
            origin=origin,
            call_id__in=month_end_ids
        )
        total = data.aggregate(price=Sum('price'))['price']
        serializer = TelephoneCallSerializer(data, many=True)
        
        return Response(
            {
                'calls': serializer.data,
                'total': total,
            }
        )


class DocsView(APIView):
    """
    PhoneBill RESTFul API URLs Documentation

    For view all documentation: http://telephonebill.herokuapp.com/
    """
    permission_classes = [AllowAny]
    def get(self, request, *args, **kwargs):
        apidocs = {'records': request.build_absolute_uri('records/'),
                   'calls': request.build_absolute_uri('calls/'),
                   'bills': request.build_absolute_uri('bills/'),
                   }
        return Response(apidocs)