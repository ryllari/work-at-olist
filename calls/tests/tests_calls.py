from django.test import TestCase, Client
from rest_framework.reverse import reverse

from .tests_utils import UserMixin


class TestCalls(TestCase, UserMixin):
    """
    Test cases in requests to TelephoneCallViewSet
    And TelephoneCall serializer and price rules
    """
    def setUp(self):
        self.create_logged_user(is_superuser=True)

    def test_not_allowed_create_telephone_call_in_post(self):
        """
        Given I'm looged user
        When I try post in telephonecall-list url
        Then I will get a Bad Request Response with "not allowed" message
        """
        response = self.client.post(
            reverse('api:telephonecall-list'),
        )

        self.assertContains(
            response,
            'not allowed',
            status_code=405
        )
        
    def test_created_call_when_end_record_is_created(self):
        """
        Given I have a call started (CallStartRecord created)
        When I ended my call (CallEndRecord created)
        My TelephoneCall to this call will be created
        """
        from datetime import datetime
        from model_mommy import mommy
        from calls.models import CallRecord, TelephoneCall

        mommy.make(
            CallRecord,
            record_type='start',
            record_time=datetime(2018, 2, 28, 1),
            call_id=1,
            origin='1123456789',
            destination='9987654321'
        )

        before = TelephoneCall.objects.count()
        
        response = self.client.post(
            reverse('api:callrecord-list'),
            {
                'record_type': 'end',
                'record_time': '2018-02-28T21:57:13',
                'call_id': '1',
            }
        )

        after = TelephoneCall.objects.count()
        self.assertEqual(before+1, after)

    def test_price_when_same_day_standard_charge(self):
        """
        Given I have a call started at 2016-02-29T12:00:00 (Standard charge)
        And ended at 2016-02-29T14:00:00 (Standard charge)
        My TelephoneCall will be the expected price:
        Std Charge: 0.36
        Call/Min: 0.09 * 120 min
        Expected price: 11.16
        """
        from datetime import datetime
        from model_mommy import mommy
        from calls.models import CallRecord, TelephoneCall

        mommy.make(
            CallRecord,
            record_type='start',
            record_time=datetime(2016, 2, 29, 12),
            call_id=1,
            origin='99988526423',
            destination='9987654321'
        )
        
        response = self.client.post(
            reverse('api:callrecord-list'),
            {
                'record_type': 'end',
                'record_time': '2016-02-29T14:00:00Z',
                'call_id': '1',
            }
        )
        expected = 11.16

        call = TelephoneCall.objects.get(call_id=1)
        self.assertEqual(float(call.price), expected)

    def test_price_when_same_day_start_and_end_in_reduced_charge(self):
        """
        Given I have a call started at 2017-12-12T22:47:56Z (Reduced Charge)
        And ended at 2017-12-12T22:50:56Z (Reduced Charge)
        My TelephoneCall will be the expected price:
        Std Charge: 0.36
        Call/Min: 0.0 * 2 min
        Expected price: 0.36
        """
        from datetime import datetime
        from model_mommy import mommy
        from calls.models import CallRecord, TelephoneCall

        mommy.make(
            CallRecord,
            record_type='start',
            record_time=datetime(2017, 12, 12, 22, 47, 56),
            call_id=1,
            origin='99988526423',
            destination='9987654321'
        )
        
        response = self.client.post(
            reverse('api:callrecord-list'),
            {
                'record_type': 'end',
                'record_time': '2017-12-12T22:50:56Z',
                'call_id': '1',
            }
        )
        expected = 0.36

        call = TelephoneCall.objects.get(call_id=1)
        self.assertEqual(float(call.price), expected)

    def test_price_when_start_standard_and_end_reduced_charge(self):
        """
        Given I have a call started at 2017-12-12T21:57:13Z (Standard Charge)
        And ended at 2017-12-12T22:10:56Z (Reduced Charge)
        My TelephoneCall will be the expected price:
        Std Charge: 0.36
        Call/Min: 0.09 * 2 min
        Expected price: 0.54
        """
        from datetime import datetime
        from model_mommy import mommy
        from calls.models import CallRecord, TelephoneCall

        mommy.make(
            CallRecord,
            record_type='start',
            record_time=datetime(2017, 12, 12, 21, 57, 13),
            call_id=1,
            origin='1123456789',
            destination='9987654321'
        )
        
        response = self.client.post(
            reverse('api:callrecord-list'),
            {
                'record_type': 'end',
                'record_time': '2017-12-12T22:10:56Z',
                'call_id': '1',
            }
        )

        expected = 0.54
        call = TelephoneCall.objects.get(call_id=1)
        self.assertEqual(float(call.price), expected)


    def test_price_when_start_reduced_an_end_standard_charge(self):
        """
        Given I have a call started at 2017-12-12T04:57:13Z (Reduced Charge)
        And ended at 2017-12-12T06:10:56Z (Standard Charge)
        My TelephoneCall will be the expected price:
        Std Charge: 0.36
        Call/Min: 0.09 * 10 min
        Expected price: 1.26
        """
        from datetime import datetime
        from model_mommy import mommy
        from calls.models import CallRecord, TelephoneCall

        mommy.make(
            CallRecord,
            record_type='start',
            record_time=datetime(2017, 12, 12, 4, 57, 13),
            call_id=1,
            origin='1123456789',
            destination='9987654321'
        )
        
        response = self.client.post(
            reverse('api:callrecord-list'),
            {
                'record_type': 'end',
                'record_time': '2017-12-12T06:10:56Z',
                'call_id': '1',
            }
        )

        expected = 1.26
        call = TelephoneCall.objects.get(call_id=1)
        self.assertEqual(float(call.price), expected)

    def test_price_when_diff_day_start_and_end_in_reduced_charge(self):
        """
        Given I have a call started at 2017-12-12T22:47:56Z (Reduced Charge)
        And ended at 2017-12-13T22:50:56Z (Reduced Charge)
        My TelephoneCall will be the expected price:
        Std Charge: 0.36
        Call/Min: 0.09 * 960 min
        Expected price: 86.76
        """
        from datetime import datetime
        from model_mommy import mommy
        from calls.models import CallRecord, TelephoneCall

        mommy.make(
            CallRecord,
            record_type='start',
            record_time=datetime(2017, 12, 12, 22, 47, 56),
            call_id=1,
            origin='99988526423',
            destination='9987654321'
        )
        
        response = self.client.post(
            reverse('api:callrecord-list'),
            {
                'record_type': 'end',
                'record_time': '2017-12-13T22:50:56Z',
                'call_id': '1',
            }
        )
        expected = 86.76

        call = TelephoneCall.objects.get(call_id=1)
        self.assertEqual(float(call.price), expected)


    def test_price_when_limits(self):
        """
        Given I have a call started at 2017-12-12T06:00:00Z (Limit)
        And ended at 2017-12-12T22:00:00Z (Limit)
        My TelephoneCall will be the expected price:
        Std Charge: 0.36
        Call/Min: 0.09 * 960 min
        Expected price: 86.76
        """
        from datetime import datetime
        from model_mommy import mommy
        from calls.models import CallRecord, TelephoneCall

        mommy.make(
            CallRecord,
            record_type='start',
            record_time=datetime(2018, 2, 28, 6),
            call_id=1,
            origin='1123456789',
            destination='9987654321'
        )
        
        response = self.client.post(
            reverse('api:callrecord-list'),
            {
                'record_type': 'end',
                'record_time': '2018-02-28T22:00:00',
                'call_id': '1',
            }
        )

        expected = 86.76

        call = TelephoneCall.objects.get(call_id=1)
        self.assertEqual(float(call.price), expected)

    def test_get_all_calls(self):
        """
        Given the Database has TelephoneCall objects stored
        When I try get telephonecall-list url
        Then I will view all calls in response
        """
        from calls.models import TelephoneCall
        from model_mommy import mommy

        mommy.make(
            TelephoneCall,
            _quantity=5,
        )
        response = self.client.get(reverse('api:telephonecall-list'))
        calls = TelephoneCall.objects.all()

        for call in calls:
            self.assertContains(response, call.call_id)
