from django.test import TestCase, Client
from rest_framework.reverse import reverse

from .tests_utils import UserMixin


class TestTelephoneBill(TestCase, UserMixin):
    """
    Test cases in requests to TelephoneBillView
    """
    def setUp(self):
        self.create_logged_user(is_superuser=True)
        self.create_calls()

    def create_calls(self):
        """
        Create records and telephone calls to be stored on test DB
        """
        from datetime import datetime
        from model_mommy import mommy
        from calls.models import CallRecord

        mommy.make(
            CallRecord,
            record_type='start',
            record_time=datetime(2018, 3, 29),
            origin='1123456789',
            destination='9987654321'
        )

        c_ids = CallRecord.objects.values_list('call_id', flat=True)
        
        for c_id in c_ids:
            self.client.post(
                reverse('api:callrecord-list'),
                {
                    'record_type': 'end',
                    'record_time': '2018-03-30T14:00:00Z',
                    'call_id': f'{c_id}',
                }
            )

    def test_show_message_on_get(self):
        """
        Given I'm logged user 
        When I try get on telephonebill url
        Then I will get a message "Provide Number to access the Telephone Bill"
        """ 
        response = self.client.get(reverse('api:telephonebill'))

        self.assertContains(
            response,
            'Provide Number to access the Telephone Bill'
        )

    def test_show_last_month_without_month_data_request(self):
        """
        Given I'm logged user 
        When I try post on telephonebill url
        And I provided just origin number, without period
        Then I will get the last month TelephoneBill
        """ 
        from calls.models import TelephoneCall, CallRecord
        from datetime import datetime
        from model_mommy import mommy
        
        # Create date to valid this test with last month
        start = datetime.now()
        c = mommy.make(
            CallRecord,
            record_type='start',
            record_time=datetime(start.year, start.month-1, start.day),
            origin='1123456789',
            destination='9987654321'
        )
        self.client.post(
            reverse('api:callrecord-list'),
            {
                'record_type': 'end',
                'record_time': str(c.record_time),
                'call_id': f'{c.call_id}',
            }
        )
        

        response = self.client.post(
            reverse('api:telephonebill'),
            {
                'origin': '1123456789',
            }
        )

        c_ids = CallRecord.objects.filter(
            record_type='end',
            record_time__date__month=start.month-1,
            record_time__date__year=start.year-1
        ).values_list('call_id', flat=True)

        calls = TelephoneCall.objects.filter(
            origin='1123456789',
            call_id__in=c_ids
        ).values_list('call_id', flat=True)

        for call_id in calls:
            self.assertContains(response, call_id)
    

    def test_show_correctly_period(self):
        """
        Given I'm logged user 
        When I try post on telephonebill url
        And I provided just origin number and period
        Then I will get the correctly month TelephoneBill
        """ 
        from calls.models import CallRecord, TelephoneCall

        response = self.client.post(
            reverse('api:telephonebill'),
            {
                'origin': '1123456789',
                'period': '03/2018'
            }
        )
        c_ids = CallRecord.objects.filter(
            record_type='end',
            record_time__date__month=3,
            record_time__date__year=2018
        ).values_list('call_id', flat=True)

        c_ids = TelephoneCall.objects.filter(
            origin='1123456789',
            call_id__in=c_ids
        ).values_list('call_id', flat=True)
        for call in c_ids:
            self.assertContains(response, call)
        
    def test_post_without_origin(self):
        """
        Given I'm logged user 
        When I try post on telephonebill url
        And I not provided the origin number
        Then I will get a Bad Request Response
        With "provide a valid origin" message
        """ 

        response = self.client.post(reverse('api:telephonebill'))
        self.assertContains(
            response,
            "Please, provide a valid origin!",
            status_code=400
        )

    def test_post_with_incorrect_period(self):
        """
        Given I'm logged user 
        When I try post on telephonebill url
        And I provided a wrong period
        Then I will get a Bad Request Response
        With "provide a valid period!" message
        """ 
        response = self.client.post(
            reverse('api:telephonebill'),
            {
                'origin': '1123456789',
                'period': '032018'
            }
        )

        self.assertContains(
            response,
            "Please, provide a valid period!",
            status_code=400
        )