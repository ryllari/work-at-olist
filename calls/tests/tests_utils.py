class UserMixin(object):
    
    user_kw = {
        'username': 'user',
        'email': 'myuser@user.com',
        'password': 'pass123',
    }

    @staticmethod
    def create_user(**kwargs):
        from django.contrib.auth import get_user_model

        User = get_user_model()
        return User.objects.create_user(**kwargs)

    def create_logged_user(self, **kwargs):
        """
        Creates a logged user

        :param kwargs:
        :return: new user
        """

        kw = self.user_kw.copy()
        kw.update(kwargs)

        user = self.create_user(**kw)

        self.client.login(
            username=kw['username'],
            password=kw['password']
        )
            
        return user