from django.test import TestCase, Client
from rest_framework.reverse import reverse

from .tests_utils import UserMixin


class TestRecords(TestCase, UserMixin):
    """
    Test cases in requests to CallRecordViewSet
    And CallRecord serializer (start and end)
    """
    def setUp(self):
        self.create_logged_user(is_superuser=True)

    def test_add_record_with_record_type_wrong(self):
        """
        Given I'm logged user 
        When I try post a new record
        And this record has a wrong type inserted
        Then I will get a Bad Request Response with "Unknown Type" message
        """
        response = self.client.post(
            reverse('api:callrecord-list'),
            {'record_type': 'error'}
        )
        
        self.assertContains(response, 'Unknown Type', status_code=400)

    def test_add_start_record_with_origin_wrong(self):
        """
        Given I'm logged user
        When I try post a new start record
        And this record has a wrong origin number inserted
        Then I will get a Bad Request Response with "Number Invalid!" message
        """
        response = self.client.post(
            reverse('api:callrecord-list'),
            {
                'record_type': 'start',
                'record_time': '2018-02-28T21:57:13',
                'call_id': '1',
                'origin': '99999999',
                'destination': '1123456789'
            }
        )
        
        self.assertContains(response, 'Number Invalid!', status_code=400)
    
    def test_add_start_record_with_destination_wrong(self):
        """
        Given I'm logged user
        When I try post a new start record
        And this record has a wrong destination number inserted
        Then I will get a Bad Request Response with "Number Invalid!" message
        """
        response = self.client.post(
            reverse('api:callrecord-list'),
            {
                'record_type': 'start',
                'record_time': '2018-02-28T21:57:13',
                'call_id': '1',
                'origin': '9999999999',
                'destination': '1123456'
            }
        )
        
        self.assertContains(response, 'Number Invalid!', status_code=400)

    def test_add_start_record_with_existent_call_id(self):
        """
        Given I'm logged user
        When I try post a new start record
        And this record has a existent call_id
        Then I will get a Bad Request Response 
        With "call_id must make a unique set" message
        """
        from calls.models import CallRecord
        from model_mommy import mommy

        mommy.make(CallRecord, record_type='start', call_id=1)

        response = self.client.post(
            reverse('api:callrecord-list'),
            {
                'record_type': 'start',
                'record_time': '2018-02-28T21:57:13',
                'call_id': '1',
                'origin': '9999999999',
                'destination': '1123456789'
            }
        )

        self.assertContains(
            response,
            'call_id must make a unique set',
            status_code=400
        )

    def test_add_start_record_success(self):
        """
        Given I'm logged user
        When I try post a new start record
        And this record has all correct fields
        Then I will get a created response
        And the CallRecords count will be increased
        """
        from calls.models import CallRecord
        before = CallRecord.objects.count()

        response = self.client.post(
            reverse('api:callrecord-list'),
            {
                'record_type': 'start',
                'record_time': '2018-02-28T21:57:13',
                'call_id': '1',
                'origin': '9999999999',
                'destination': '1123456789'
            }
        )

        after = CallRecord.objects.count()

        self.assertEqual(before+1, after)
        self.assertEqual(response.status_code, 201)

    def test_add_end_record_with_inexistent_start_record(self):
        """
        Given I'm logged user
        When I try post a new end record
        And this record has a inexistent call_id on start records
        Then I will get a Bad Request Response 
        With "CallStartRecord with this call_id not found" message
        """
        response = self.client.post(
            reverse('api:callrecord-list'),
            {
                'record_type': 'end',
                'record_time': '2018-02-28T21:57:13',
                'call_id': '1'
            }
        )

        self.assertContains(
            response,
            "CallStartRecord with this call_id not found",
            status_code=400
        )

    def test_add_end_record_with_time_invalid(self):
        """
        Given I'm logged user
        When I try post a new end record
        And this record has a record_time less than the start record_time
        Then I will get a Bad Request Response with "Time Invalid" message
        """
        from datetime import datetime
        from model_mommy import mommy
        from calls.models import CallRecord

        mommy.make(
            CallRecord,
            record_type='start',
            record_time=datetime(2018, 3, 2),
            call_id=1,
            origin='1123456789',
            destination='9987654321'
        )
        response = self.client.post(
            reverse('api:callrecord-list'),
            {
                'record_type': 'end',
                'record_time': '2018-02-28T21:57:13',
                'call_id': '1'
            }
        )
        
        self.assertContains(
            response,
            "Time Invalid!",
            status_code=400
        )

    def test_add_end_record_success(self):
        """
        Given I'm logged user
        When I try post a new end record
        And this record has all correct fields
        Then I will get a created response
        And the CallRecords count will be increased
        """
        from datetime import datetime
        from model_mommy import mommy
        from calls.models import CallRecord

        mommy.make(
            CallRecord,
            record_type='start',
            record_time=datetime(2018, 2, 28, 1),
            call_id=1,
            origin='1123456789',
            destination='9987654321'
        )

        before = CallRecord.objects.count()
        response = self.client.post(
            reverse('api:callrecord-list'),
            {
                'record_type': 'end',
                'record_time': '2018-02-28T21:57:13',
                'call_id': '1',
            }
        )

        after = CallRecord.objects.count()

        self.assertEqual(before+1, after)
        self.assertEqual(response.status_code, 201)

    def test_get_all_records(self):
        """
        Given the Database has CallRecord objects stored
        When I try get callrecord-list url
        Then I will view all records in response
        """
        from calls.models import CallRecord
        from model_mommy import mommy

        mommy.make(
            CallRecord,
            _quantity=5,
        )
        response = self.client.get(reverse('api:callrecord-list'))
        records = CallRecord.objects.all()

        for record in records:
            self.assertContains(response, record.call_id)