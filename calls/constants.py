CALL_START = 'start'
CALL_END = 'end'

TYPE_CHOICES = (
    (CALL_START, 'start'),
    (CALL_END, 'end'),
)

STAND_CHARGE = 0.36
CALL_CHARGE = 0.09
