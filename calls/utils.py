from datetime import datetime

from calls.constants import STAND_CHARGE, CALL_CHARGE
from calls.models import CallRecord


def get_telephone_call(call_id):
	"""
	This function search and organize all data about a TelephoneCall

	:param int call_id:
	:return dict call: Contains all values to create a TelephoneCall
	"""
	call = {'call_id': call_id}
	start = CallRecord.objects.get(call_id=call_id, record_type='start')
	end = CallRecord.objects.get(call_id=call_id, record_type='end')

	duration, minutes = get_duration_minutes(start.record_time, end.record_time)
	price = STAND_CHARGE + CALL_CHARGE * minutes
	call['origin'] = start.origin
	call['destination'] = start.destination
	call['start_date'] = str(start.record_time.date())
	call['start_time'] = str(start.record_time.time())
	call['duration'] = duration
	call['price'] = price

	return call

def get_duration_minutes(start, end):
	"""
	This function calculates the duration and the price of a TelephoneCall
	The price is calculated according to the requirements requested
	So, the minutes between 10pm and 6am each day are not calculated

	:param datetime start:
	:param datetime end:
	:returns timedelta duration, float price:
	"""
	initial = 0
	finish = 0
	
	duration = end-start

	if start.date() != end.date():
		if 6 <= start.hour < 22:
			initial = datetime(
				start.year,
				start.month,
				start.day,
				22,
				tzinfo=start.tzinfo
			) - start
			initial = initial.total_seconds()	
	
		if 6 <= end.hour < 22:
			finish = end - datetime(
				end.year,
				end.month,
				end.day,
				6,
				tzinfo=end.tzinfo
			)
			finish = finish.total_seconds()
		
		minutes = int((initial + finish) / 60) + (duration.days * 16 * 60)
	
	else:
		if start.hour < 6:
			initial = 6 * 60 * 60
		elif start.hour < 22:
			initial = start.hour * 3600 + (start.minute * 60) + start.second
		elif start.hour == 22:
			initial = 22 * 3600

		if end.hour >= 22:
			finish = 22 * 60 * 60
		elif end.hour >= 6:
			finish = (end.hour * 60  * 60) + (end.minute * 60) + end.second

		minutes = int((finish - initial) / 60)
	
	return duration, minutes
