from django.contrib import admin

from calls.models import CallRecord, TelephoneCall

admin.site.register(CallRecord)
admin.site.register(TelephoneCall)