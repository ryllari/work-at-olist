from django.urls import path, include
from rest_framework import routers

from calls.views import CallRecordViewSet, TelephoneCallViewSet, TelephoneBillView, DocsView

router = routers.DefaultRouter()
router.register('calls', TelephoneCallViewSet)
router.register('records', CallRecordViewSet)

app_name = 'calls'

urlpatterns = [
    path('bills/', TelephoneBillView.as_view(), name='telephonebill'),
    path('', DocsView.as_view()),
    path('', include(router.urls)),
] 
