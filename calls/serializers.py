from rest_framework import serializers

from calls.models import CallRecord, TelephoneCall
from calls.utils import get_telephone_call


class CallStartSerializer(serializers.ModelSerializer):
    """
    This class serializer a CallStartRecord
    A CallStartRecord is a CallRecord object
    That register call start informations
    with our fields 'origin' and 'destination' required
    """
    class Meta:
        model = CallRecord
        fields = '__all__'
        extra_kwargs = {
            'origin': {
                'allow_blank': False,
                'allow_null': False,
                'required': True
                },
            'destination': {
                'allow_blank': False,
                'allow_null': False,
                'required': True
                },
            }


class CallEndSerializer(serializers.ModelSerializer):
    """
    This class serializer a CallEndRecord
    A CallEndRecord is a CallRecord object
    That register call end informations
    And without 'origin' and 'destination' fields
    """
    class Meta:
        model = CallRecord
        fields = ['id', 'call_id', 'record_type', 'record_time']

    def validate(self, data):
        """
        Validate a CallEndRecord
        The validation includes:
        - Verify if exists a CallStartRecord with the call_id informed
        - Verify if the record_time is valid (> CallStart's record_time)

        :param data: serializer data to be validated
        :return data:

        """
        call_id = data['call_id'] 
        try:
            start = CallRecord.objects.get(
                call_id=call_id,
                record_type='start'
            )
        except:
            raise serializers.ValidationError(
                "CallStartRecord with this call_id not found!"
            )

        if start.record_time > data['record_time']:
            raise serializers.ValidationError("Time Invalid!")

        return data

    def create(self, validated_data):
        """
        Create a EndRecord
        When a call is ended (CallEndRecord is created)
        A TelephoneCall is created

        :param validated_data:
        :return obj:
        """
        obj = CallRecord.objects.create(**validated_data)
        params = get_telephone_call(obj.call_id)
        TelephoneCall.objects.create(**params)
        
        return obj


class TelephoneCallSerializer(serializers.ModelSerializer):
    """
    This class serializer a CallStartRecord
    The serializer exclude the fields 'id' and 'origin'
    """
    class Meta:
        model = TelephoneCall
        exclude = ['id', 'origin']
