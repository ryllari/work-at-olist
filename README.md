
# PhoneBill API Documentation

Base URL: `https://telephonebill.herokuapp.com/`
If you want access API from your browser, [click here](https://telephonebill.herokuapp.com/api/).
The challenge's requirements can be found [here](https://github.com/olist/work-at-olist)

The PhoneBill API is based on three services:
- List and Register Call Record, based on CallStart and CallEnd records
- List TelephoneCall, that contains informations about a CallRecord, as price and duration
- List TelephoneBill, that contains informations about a number's telephone bill, according the reported period  

CallRecord List
-
Returns all existing records.

**URL:** `/api/records/`

**HTTP method:** `GET`

**URL params:** None

**Success response**
 * HTTP Status Code: 200
 * Content:
 
   ```json
	{
	    "call_starts": [
	        {
	            "id": 1,
	            "record_type": "start",
	            "record_time": "2018-02-28T22:10:56Z",
	            "call_id": 1,
	            "origin": "99988526423",
	            "destination": "9993468278"
	        }
	    ],
	    "call_ends": [
	        {
	            "id": 2,
	            "call_id": 1,
	            "record_type": "end",
	            "record_time": "2018-02-28T22:50:56Z"
	        }
	    ]
	}
   ```

CallRecord Create
-
Create a record according the record_type.

**URL:** `/api/records/`

**HTTP method:** `POST`

**URL params:**
* `call_id` (int) - required
* `record_type` (str) - required, must be `'start'` or `'end'`
* `record_time` (timestamp) - required
* `origin` (str) - exists and required only CallStartRecord creation, must be a number with 10-11 characters
* `destination` (str) - exists and required only CallStartRecord creation, must be a number with 10-11 characters

**Success response**
* HTTP Status Code: 201
* Content:

  ```json
	{
	    "id": 2,
	    "call_id": 1,
	    "record_type": "end",
	    "record_time": "2018-02-28T22:50:56Z"
	}
  ```

**Error Responses**
In addition to the required field errors, we find the following errors:
* Invalid record_type.
	* HTTP Status Code: 400
	* Content:
	  ```json
	  "Unknown Type"
	  ```

* Invalid origin or destination
	* HTTP Status Code: 400
	* Content:
	 ```json
		{
		    "origin": [
		        "Number Invalid!         The number format is AAXXXXXXXXX, where AA is the area code         and XXXXXXXXX is the phone number.         The phone number is composed of 8 or 9 digits."
			    ]
	    }
	 ```

* Invalid call_id on CallEndRecord
	* HTTP Status Code: 400
	* Content:
	 ```json
		{
		    "non_field_errors": [
		        "CallStartRecord with this call_id not found!"
		    ]
		}
	 ```

* Invalid record_time on CallEndRecord
	* HTTP Status Code: 400
	* Content:
	 ```json
		{
		    "non_field_errors": [
		        "Time Invalid!"
		    ]
		}
	 ```

CallRecord Detail
-
Returns a record detail.

**URL:** `/api/records/<id>/`

**HTTP method:** `GET`

**URL params:**
* `id` (int)

**Success response**
* HTTP Status Code: 200
* Content:

  ```json
	{
	    "id": 1,
	    "record_type": "start",
	    "record_time": "2018-02-28T22:10:56Z",
	    "call_id": 1,
	    "origin": "99988526423",
	    "destination": "9993468278"
	}
  ```

**Error Response**
* Invalid record ID.
	* HTTP Status Code: 404
	* Content:
	  ```json
	  {
	    "detail":  "Not found."
	  }
	  ```


TelephoneCall List
-
Returns all existing calls.

**URL:** `/api/calls/`

**HTTP method:** `GET`

**URL params:** None

**Success response**
* HTTP code: 200
* Content:

  ```json
	[
	    {
	        "call_id": 1,
	        "destination": "9993468278",
	        "start_date": "2018-02-28",
	        "start_time": "22:10:56",
	        "duration": "00:40:00",
	        "price": "0.36"
	    }
	]  
	```

TelephoneCall Detail
-
Returns a TelephoneCall detail.

**URL:** `/api/calls/<call_id>`

**HTTP method:** `GET`

**URL params:** 
* `call_id` (int)

**Success response**
* HTTP code: 200
* Content:

  ```json
    {
        "call_id": 1,
        "destination": "9993468278",
        "start_date": "2018-02-28",
        "start_time": "22:10:56",
        "duration": "00:40:00",
        "price": "0.36"
    }
	```

**Error Response**
* Invalid call_id.
	* HTTP Status Code: 404
	* Content:
	  ```json
	  {
	    "detail":  "Not found."
	  }
	  ```

TelephoneBill List (GET)
-
Returns a message to make a POST.

**URL:** `/api/bills/`

**HTTP method:** `GET`

**URL params:** None

**Success response**
* HTTP code: 200
* Content:

  ```json   
	{
	    "message": "Provide Number to access the Telephone Bill"
	}
	```

TelephoneBill List (POST)
-
Returns a message to make a POST.

**URL:** `/api/bills/`

**HTTP method:** `POST`

**URL params:** 
* origin (int) - required
* period (str) -  if not provided, will be used the last month

**Success response**
* HTTP code: 200
* Content:

  ```json   
	{
	    "calls": [
	        {
	            "call_id": 70,
	            "destination": "9993468278",
	            "start_date": "2016-02-29",
	            "start_time": "12:00:00",
	            "duration": "02:00:00",
	            "price": "11.16"
	        }
	    ],
	    "total": 11.16
	}
	```

**Error Responses**
* Invalid period.
	* HTTP Status Code: 400
	* Content:
	  ```json
	  "Please, provide a valid period! (format: MM/YYYY)"
	  ```

* Invalid origin.
	* HTTP Status Code: 400
	* Content:
	  ```json
	  "Please, provide a valid origin!"
	  ```

# More Informations
* You can use the username and password `olist` as credentials. Use the admin URL to login
* All code are document with docstrings (included the tests)
* For install, execute:
	```
	$ pip install -r requirements.txt
	$ ./manage.py migrate
	$ ./manage.py collectstatic
	$ ./manage.py runserver
	```
* For run tests, execute: `./manage.py test`

# Work environment

* Computer: Samsung Series 7 Ultra, Core i7, 8 GB RAM
* Operating system: Ubuntu 18.04 LTS
* Text editors: VS Code
* Python version: 3.6.5
* Django version: 2.1.1
* Libraries: django-rest-framework

